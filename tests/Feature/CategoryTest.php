<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Category;
use Faker\Generator as Faker;

class CategoryTest extends TestCase
{
    use WithFaker;

    public function test_fetch_all_category_for_a_user()
    {
        $this->getJson(route('api.categories.index'))->assertOk()->json('data');
    }

    public function test_required_fields()
    {
        $this->postJson(route('api.categories.store'))
            ->assertJsonStructure([ "code", "message", "data" => []])
            ->assertStatus(422);
    }

    public function test_user_can_create_new_category()
    {
        $category = Category::factory()->raw();

        $this->postJson(route('api.categories.store'), $category)
            ->assertJsonStructure(['data' => ['id', 'name', 'is_publish', 'created_at',],'code','message',])
            ->assertCreated()
            ->assertStatus(201);
    }
    
    public function test_user_can_update_category()
    {
        $category = Category::factory()->create();

        $this->patchJson(route('api.categories.update', $category->id), ['name' => 'test updated','is_publish' => 1])
            ->assertJsonStructure(['data' => ['id', 'name', 'is_publish', 'created_at'],'code','message'])
            ->assertOk();
    }

    public function test_user_can_delete_a_category()
    {
        $category = Category::factory()->create();
        
        $this->deleteJson(route('api.categories.destroy', $category->id))
            ->assertJsonStructure(['code','message'])
            ->assertStatus(200);
    }
}
