<?php

namespace App\Repositories\Contracts;

Interface BaseRepositoryInterface
{
    public function paginate($limit = null, $columns = ['*']);

    public function simplePaginate($limit = null, $columns = ['*']);

    public function find($id);

    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function delete($id);
}
