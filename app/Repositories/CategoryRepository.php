<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\Contracts\CategoryInterface;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryInterface
{
    protected $model;

    public function __construct(Category $category)
    {
        $this->model = $category->whereNotNull('id');
    }

    public function getAllSimplePaginatedWithParams($params, $limit = 10)
    {
        $categories = $this->model;
        if(isset($params->search) && !empty($params->search)) $categories->where('name', 'like', '%' . $params->search . '%');
        $categories = $categories->orderBy('id', 'DESC')->simplePaginate($limit);
        return $categories;
    }

    public function getAllPaginatedWithParams($params, $limit = 10)
    {
        $categories = $this->model;
        if(isset($params->search) && !empty($params->search)) $categories->where('name', 'like', '%' . $params->search . '%');
        $categories = $categories->orderBy('id', 'DESC')->paginate($limit);
        return $categories;
    }
}
